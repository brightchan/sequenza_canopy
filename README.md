# Sequenza and Canopy #

This package aims to parse and to collect info from sequenza analyses and raw files of multiple tumor regions (from the same patient) and prepare the data to run Canopy.
The packaging is convenient to distribute and tide up the code, but this will probably never be submitted to CRAN or BioC.

__to install__

```
library(devtools)
install_bitbucket("sequenzatools/sequenza_canopy")
```

__Run example__

Some lines from the 2 requred files:

trusted/selected mutations ("selected_mutations_file.txt"), 3-columns tab separated file, without header:

```
1	45268689	PLK3
14	106866426	IGHV3-38
16	2358525	ABCA3
16	87380809	FBXO31
17	27208883	FLOT2
...
```

and

trusted/selected segments ("selected_segments_file.txt"), 4-columns tab separated file, without header:

```
2	165000000	240000000	chr2q
8	42000000	 60000000	chr8c
10	80000000	88000000	chr10q1
10	88000000	95000000	chr10pten
...
```



```
library(sequenzaCanopy)

patient_id <- "patient_test"

results_dir <-  file.path(getwd(), paste(patient_id, "canopy", sep = "_"))
dir.create(results_dir)

## Assuming that each sequenza results folder for each tumor sector are
## in the same output directory patient_id is in included in each folder name
## we can get the list of sequenza results by using dir

patient_dir <- dir(patient_dir, pattern = patient_id, full.names = TRUE)

## Similarly we can get the list of seqz files

seqz_files <- dir(file.path(base_dir, "sqz", "all", "covs"),
    pattern = patient_id, full.names = TRUE)

# We also need to load the previously selected regions and mutations

selected_mutations <- read.table("selected_mutations_file.txt", header = FALSE,
    sep = "\t", stringsAsFactors = FALSE)
selected_segments <- read.table("selected_segments_file.txt", header = FALSE,
    sep = "\t", stringsAsFactors = FALSE)

# Now we can load the regions from sequenza results

patient <- import_patient(patient_dir)

# If you don't have the "selected_segments_file.txt" yet, you can inspect the
# sector chromosomes, to locate the deleted/amplified regions in one
# or more of the sectors

inspect_cna_chromosome(sectors = patient, chr = "1")

# If you want to display the selected regions over the samples

pdf(file.path(results_dir, paste(patient_id, "scna_inspect.pdf", sep = "_")))
    for (i in as.character(c(1:22, "X", "Y"))) {
        inspect_cna_chromosome(patient, i, breaks = selected_segments)
    }
dev.off()


# Similarly with the mutations

depths <- mutations_depths(patient, selected_mutations)

# Although we need to fix the mutations where the depths$tot is equal to 0
# to avoid having zeros denominator

depths$tot[depths$tot == 0] <-  round(mean(depths$tot[depths$tot > 0]), 0)

# Display the frequency of each mutation in the various sectors

pdf(file.path(results_dir,
        paste(patient_id, "mutation_freq_heatmap.pdf", sep = "_")),
    width = 8, height = 4)
    freq <- depths$alt / depths$tot
    par(mar = c(8, 8, 2, 2))
    colorgram(freq, xaxt = "n", yaxt = "n", ylab = NA, xlab = NA)
    axis(side = 2, at = 1:ncol(freq), labels = colnames(freq), las = 1)
    axis(side = 1, at = 1:nrow(freq), labels = rownames(freq), las = 2)
dev.off()

# The seqz files should be a named vector, each seqz path named width
# the corresponding sector name. If the sector name is include in the
# seqz file name, it's possible to use the function  'seqz_names'

seqz_files <- seqz_names(seqz_files, patient)

# Now we can finally run Canopy!!

ss <- segments_values(seqz_files, selected_segments)

Y <- check_overlaps(selected_mutations[
    selected_mutations[, 3] %in% rownames(depths$alt), ],
    selected_segments)


projectname <- patient_id
R <- depths$alt
X <- depths$tot
WM <- ss$WM
Wm <- ss$Wm
epsilon <- 0.02

K = 3:6; numchain = 20
sampchain = canopy.sample(R = R, X = X,
                          WM = WM, Wm = Wm,
                          epsilonM = epsilon, epsilonm = epsilon, C = NULL,
                          Y = Y, K = K, numchain = numchain,
                          simrun = 10000, writeskip = 200,
                          projectname = projectname, cell.line = FALSE,
                          plot.likelihood = TRUE)

burnin = 10
thin = 10
# If pdf = TRUE, a pdf will be generated.
bic = canopy.BIC(sampchain = sampchain, projectname = projectname, K = K,
               numchain = numchain, burnin = burnin, thin = thin, pdf = TRUE)
optK = K[which.max(bic)]

#optK <- 5

post = canopy.post(sampchain = sampchain, projectname = projectname, K = K,
               numchain = numchain, burnin = burnin, thin = thin,
               optK = optK, post.config.cutoff = 0.05)


samptreethin = post[[1]]   # list of all post-burnin and thinning trees
samptreethin.lik = post[[2]]   # likelihoods of trees in samptree
config = post[[3]]
config.summary = post[[4]]
print(config.summary)

config.i = config.summary[which.max(config.summary[,3]),1]
cat('Configuration', config.i, 'has the highest posterior likelihood.\n')
output.tree = canopy.output(post, config.i, C=NULL)
pdf.name = paste(projectname, '_config_highest_likelihood.pdf', sep='')
txt.name = paste(projectname, '_config_highest_likelihood.txt', sep='')
canopy.plottree(output.tree, pdf = TRUE, pdf.name = pdf.name,
    txt = TRUE, txt.name = txt.name)
canopy.plottree(output.tree, pdf = FALSE)
```


## TODO:
- many things so far
